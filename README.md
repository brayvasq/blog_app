# README

Ruby version: 5.2.2

### Creando proyecto
```bash
rails _5.2.1_ new blog_app –d postgresql
```
Dependencias
```ruby
# Gemfile
## Añadir
gem 'pg'
gem 'rails_12factor', group: :production

## Borrar
gem 'sqlite3'
```
Instalando dependencias
```bash
bundle install
```
Creando la base de datos
```bash
rails db:create
rails db:setup
```
Ejecutando migraciones
```bash
rails db:migrate
```

Creando controladores
```bash
rails g controller articles index
```

Creando modelos
```bash
rails g model article title:string body:text
```
##### Añadiendo bootstrap
Añadir las gemas
```ruby
gem 'bootstrap-sass'
gem 'autoprefixer-rails'
```
Crear un archivo `.scss` y añadir
```sass
@import "bootstrap-sprockets";
@import "bootstrap";
```

Solución error Jquery
```ruby
#Añadir gema
gem 'jquery-rails'
```

```javascript
/* app/assets/javascripts/application.js */
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
```

##### Autenticación
Añadir la siguiete gema
```bash
gem 'devise', '~>4.5.0'
```
Generando los componentes de la gema
```bash
rails g devise:install
```
Generando el modelo de usuario
```bash
rails g devise user
```
Generando vistas del auth
```bash
rails g devise:views
```

### Ejecutando proyecto
Instalando dependencias
```bash
sudo apt-get install libpq-dev
bundle install
```
Creando la base de datos
```bash
rails db:create
rails db:setup
```
Ejecutando migraciones
```bash
rails db:migrate
```

Ejecutando pruebas
```bash
bundle exec rspec
```
Ejecutando
```bash
rails s --binding=0.0.0.0
```

